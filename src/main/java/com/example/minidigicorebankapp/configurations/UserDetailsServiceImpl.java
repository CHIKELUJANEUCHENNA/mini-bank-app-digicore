package com.example.minidigicorebankapp.configurations;


import com.example.minidigicorebankapp.model.Account;
import com.example.minidigicorebankapp.services.AccountService;
import com.example.minidigicorebankapp.services.accountServiceImpl.AccountServiceImpl;
import lombok.SneakyThrows;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.security.auth.login.AccountNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String accountName) throws UsernameNotFoundException {
        AccountService accountService = new AccountServiceImpl();

        Map<String, Account> accounts = accountService.getAllAccounts();

        Account account1 = new Account();
        for (Account account : accounts.values()) {
            if (account.getAccountName().equalsIgnoreCase(accountName)) {
                account1 = account;
            }
        }
        if (account1 == null) {
            throw new AccountNotFoundException("Account not found");
        } else {
            return new User(account1.getAccountName(),account1.getAccountPassword(),new ArrayList<>());
        }

    }
}
