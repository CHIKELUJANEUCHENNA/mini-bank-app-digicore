package com.example.minidigicorebankapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniDigicoreBankAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(MiniDigicoreBankAppApplication.class, args);
    }

}
