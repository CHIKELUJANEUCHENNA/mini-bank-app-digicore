package com.example.minidigicorebankapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateAccountDto {
    private String accountName;
    private String accountPassword;
    private Double initialDeposit;
}
