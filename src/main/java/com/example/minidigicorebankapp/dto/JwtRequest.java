package com.example.minidigicorebankapp.dto;

import java.io.Serializable;

public class JwtRequest implements Serializable {

    private static final long serialVersionUID = 5926468583005150707L;

    private String accountNo;
    private String password;


    public JwtRequest()
    {
    }

    public JwtRequest(String accountNo, String password) {
        this.setAccountNo(accountNo);
        this.setPassword(password);
    }

    public String getAccountNo() {
        return this.accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
