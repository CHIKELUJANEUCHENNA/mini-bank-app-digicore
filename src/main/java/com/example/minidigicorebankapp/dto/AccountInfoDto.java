package com.example.minidigicorebankapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountInfoDto {
    private String accountName;
    private String accountNumber;
    private Double balance;
}
