package com.example.minidigicorebankapp.services.accountServiceImpl;

import com.example.minidigicorebankapp.dto.*;
import com.example.minidigicorebankapp.enums.TransactionType;
import com.example.minidigicorebankapp.model.Account;
import com.example.minidigicorebankapp.model.StatementOfAccount;
import com.example.minidigicorebankapp.payload.ApiResponse;
import com.example.minidigicorebankapp.services.AccountService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Data
@Slf4j
public class AccountServiceImpl implements AccountService {
   static HashMap<String, Account> accountList = new HashMap<>();
    List<StatementOfAccount> accountStatement = new ArrayList<>();

//    public AccountServiceImpl(List<Account> accountList) {
//        this.accountList = accountList;
//    }
//
//    public AccountServiceImpl() {
//    }

    public static String randomNumber() {
        StringBuilder accountNum = new StringBuilder();
        String numericString = "0123456789";
        for (int i = 0; i < 10; i++) {
            int index = (int) (numericString.length() * Math.random());
            accountNum.append(numericString.charAt(index));
        }
        return accountNum.toString();
    }


    @Override
    public ApiResponse createAccount(CreateAccountDto createAccountDto) {
        Account account = new Account();
        ApiResponse response = new ApiResponse();
        account.setAccountName(createAccountDto.getAccountName());
        account.setAccountPassword(createAccountDto.getAccountPassword());
        account.setAccountNumber(randomNumber());
        account.setBalance(createAccountDto.getInitialDeposit());
        log.info(account.getAccountNumber());
        log.info(account.getAccountName());
        log.info(account.getBalance().toString());

        if (accountList.size() == 0) {
//            accountList = new HashMap<>();
            accountList.put(account.getAccountNumber(), account);
            System.out.println("size is 0 "+ accountList.size());
            response.setSuccess(true);
            response.setMessage("Account created successfully");
            response.setStatus(HttpStatus.OK);
        } else {
            for (Map.Entry<String, Account> acc : accountList.entrySet()) {
                if (acc.getValue().getAccountName().equalsIgnoreCase(account.getAccountName())) {
                    response.setStatus(HttpStatus.CONFLICT);
                    response.setSuccess(false);
                    response.setMessage("Account already created");
                    return response;
                }
            }
                accountList.put(account.getAccountNumber(), account);
                System.out.println("size greater than 0" + accountList);
                response.setStatus(HttpStatus.OK);
                response.setMessage("Account created successfully");
                response.setSuccess(true);


        }
        return response;

    }


    @Override
    public HashMap<String, Account> getAllAccounts() {
        return accountList;
    }

    @Override
    public AccountInfoDto accountInfo(String accountNumber) {
        AccountInfoDto accountInfoDto = new AccountInfoDto();
        System.out.println(accountList.size());
        if (accountList.containsKey(accountNumber)) {
            Account account = accountList.get(accountNumber);
            if (account.getAccountNumber().equals(accountNumber)) {
                accountInfoDto.setAccountName(account.getAccountName());
                accountInfoDto.setBalance(account.getBalance());
                accountInfoDto.setAccountNumber(account.getAccountNumber());
            }
        }
        return accountInfoDto;
    }

    @Override
    public ApiResponse deposit(DepositDto depositDto) {
        ApiResponse response = null;
        for(Account account : accountList.values()) {
            if(account.getAccountNumber().equals(depositDto.getAccountNumber())) {
                if(depositDto.getAmount() > 1000000 || depositDto.getAmount() < 1) {
                    response = new ApiResponse(Boolean.FALSE, "Limit exceeded");
                }
                else {
                    StatementOfAccount statementOfAccount = new StatementOfAccount();
                    double previousBalance = account.getBalance();
                    double currentBalance = previousBalance + depositDto.getAmount();
                    account.setBalance(currentBalance);
                    statementOfAccount.setAccountBalance(currentBalance);
                    statementOfAccount.setTransactionDate(new Date());
                    statementOfAccount.setTransactionType(TransactionType.DEPOSIT);
                    accountStatement.add(statementOfAccount);
                    response = new ApiResponse(Boolean.TRUE, "deposit successful", HttpStatus.OK);
                }
            }
            else {
                response = new ApiResponse(Boolean.FALSE, "account unavailable", HttpStatus.BAD_REQUEST);
            }
        }
        return response;
    }

    @Override
    public ApiResponse withdraw(WithdrawalDto withdrawalDto) {
        ApiResponse response = null;
        for(Account account : accountList.values()) {
            if(account.getAccountNumber().equals(withdrawalDto.getAccountNumber())) {
                if(withdrawalDto.getAmount() < 500) {
                    response = new ApiResponse(Boolean.FALSE, "insufficient balance");
                }
                else {
                    StatementOfAccount statementOfAccount = new StatementOfAccount();
                    double previousBalance = account.getBalance();
                    double currentBalance = previousBalance - withdrawalDto.getAmount();
                    account.setBalance(currentBalance);
                    statementOfAccount.setAccountBalance(currentBalance);
                    statementOfAccount.setTransactionType(TransactionType.WITHDRAWAL);
                    statementOfAccount.setTransactionDate(new Date());
                    accountStatement.add(statementOfAccount);
                    response = new ApiResponse(Boolean.TRUE, "withdrawal successful", HttpStatus.OK);
                }
            }
            else {
                response = new ApiResponse(Boolean.FALSE, "account unavailable", HttpStatus.BAD_REQUEST);
            }
        }
        return response;
    }


    @Override
    public StatementOfAccount allAccountTransactions(String accountNumber) {
        StatementOfAccount statement = new StatementOfAccount();
        for (StatementOfAccount account : accountStatement) {
            if (account.getAccountNumber().equals(accountNumber)) {
                statement = account;
            }
        }
        return statement;
    }

}