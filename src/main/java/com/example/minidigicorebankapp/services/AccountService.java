package com.example.minidigicorebankapp.services;

import com.example.minidigicorebankapp.dto.*;
import com.example.minidigicorebankapp.model.Account;
import com.example.minidigicorebankapp.model.StatementOfAccount;
import com.example.minidigicorebankapp.payload.ApiResponse;


import java.util.HashMap;

public interface AccountService {
    ApiResponse createAccount (CreateAccountDto createAccountDto);
    public HashMap<String, Account> getAllAccounts();
    public AccountInfoDto accountInfo (String accountNumber);
    public ApiResponse deposit(DepositDto depositDto);
    public ApiResponse withdraw(WithdrawalDto withdrawalDto);
    StatementOfAccount allAccountTransactions(String accountNumber);

}
