package com.example.minidigicorebankapp.controller;

import com.example.minidigicorebankapp.dto.AccountInfoDto;
import com.example.minidigicorebankapp.dto.DepositDto;
import com.example.minidigicorebankapp.dto.WithdrawalDto;
import com.example.minidigicorebankapp.model.StatementOfAccount;
import com.example.minidigicorebankapp.payload.ApiResponse;
import com.example.minidigicorebankapp.services.AccountService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AccountController {
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/account_info/{accountNumber}")
    private ResponseEntity<AccountInfoDto> accountInfo(@PathVariable String accountNumber) {
        HttpHeaders httpHeaders = new HttpHeaders();
        String message = "success";
        httpHeaders.add("message", message);
        AccountInfoDto accountInfo = accountService.accountInfo(accountNumber);
        return new ResponseEntity<>(accountInfo, HttpStatus.OK);
    }

    @PostMapping("/deposit")
    private ResponseEntity<ApiResponse> deposit(@RequestBody DepositDto depositDto) {
        ApiResponse response = accountService.deposit(depositDto);
        HttpStatus status = response.isSuccess() ? HttpStatus.CREATED : HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(response, status);
    }

    @PostMapping("/withdrawal")
    private ResponseEntity<ApiResponse> withdrawal(@RequestBody WithdrawalDto withdrawalDto) {
        ApiResponse response = accountService.withdraw(withdrawalDto);
        HttpStatus status = response.isSuccess() ? HttpStatus.CREATED : HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(response, status);
    }

    @GetMapping(value = "/account_statement/{accountNumber}")
    public ResponseEntity<StatementOfAccount> transactionHistory(@PathVariable String accountNumber){
        return new ResponseEntity<>(accountService.allAccountTransactions(accountNumber), HttpStatus.ACCEPTED);

    }
}
