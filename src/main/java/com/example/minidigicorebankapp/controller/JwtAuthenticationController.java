package com.example.minidigicorebankapp.controller;


import com.example.minidigicorebankapp.configurations.JwtTokenUtil;
import com.example.minidigicorebankapp.configurations.UserDetailsServiceImpl;
import com.example.minidigicorebankapp.dto.CreateAccountDto;
import com.example.minidigicorebankapp.dto.JwtRequest;
import com.example.minidigicorebankapp.dto.JwtResponse;
import com.example.minidigicorebankapp.payload.ApiResponse;
import com.example.minidigicorebankapp.services.AccountService;
import com.example.minidigicorebankapp.services.accountServiceImpl.AccountServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class JwtAuthenticationController {
//    @Autowired
//    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    AccountService service = new AccountServiceImpl();
@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest jwtRequest) throws Exception {

//        try {
//            System.out.println(jwtRequest.getAccountNo());
//            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken
//                    (jwtRequest.getAccountNo(), jwtRequest.getPassword()));
//        } catch (DisabledException e) {
//            throw new Exception("USER_DISABLED", e);
//        } catch (BadCredentialsException e) {
//            throw new Exception("INVALID_CREDENTIALS", e);
//        }
        final UserDetails userDetails = userDetailsService.loadUserByUsername(jwtRequest.getAccountNo());

        final String jwtToken = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(jwtToken));
    }
@RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity<ApiResponse> registerAccount(@RequestBody CreateAccountDto account) throws Exception {
       ApiResponse response = service.createAccount(account);
        HttpStatus status = response.isSuccess() ? HttpStatus.CREATED : HttpStatus.BAD_REQUEST;
    System.out.println(status);
        return new ResponseEntity<>(response, status);
    }
}
