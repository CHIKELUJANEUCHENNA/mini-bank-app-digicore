package com.example.minidigicorebankapp.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    private long id;
    private String accountName;
    private String accountNumber;
    private Double balance;
    private String accountPassword;
}
