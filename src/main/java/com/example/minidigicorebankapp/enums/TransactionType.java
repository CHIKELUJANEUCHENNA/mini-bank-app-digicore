package com.example.minidigicorebankapp.enums;

public enum TransactionType {
    DEPOSIT,
    WITHDRAWAL
}
