public class CountSquare {
    public static void main(String[] args) {
        System.out.println(countSquares(4,5));
    }
    static int countSquares(int m, int n) {
        // If n is smaller, swap m and n
        if (n < m) {
            int temp = m;
            m = n;
            n = temp;
        }
        // Now n is greater dimension,
        // apply formula
        return n * (n + 1) * (3 * m - n + 1) / 6;
    }
}
